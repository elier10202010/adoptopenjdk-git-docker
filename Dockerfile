FROM adoptopenjdk:16-jdk-openj9
RUN apt-get update \
    && apt-get -y install git \ 
    && rm -rf /var/lib/apt/lists/*
